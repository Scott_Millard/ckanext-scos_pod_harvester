This is a custom written CKAN extension that  allows CKAN to harvest
data.json files that are in the POD 1.1 format as defined by:
https://project-open-data.cio.gov/v1.1/schema/