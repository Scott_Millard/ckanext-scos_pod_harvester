from setuptools import setup, find_packages

version = '0.0.1'

setup(
	name='ckanext-scos_pod_harvester',
	version=version,
	description='SCOS POD schema harvester',
	long_description='',
	classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
	keywords='',
	author='',
	author_email='',
	url='',
	license='',
	packages=find_packages(),
	namespace_packages=['ckanext', 'ckanext.scos_pod_harvester'],
	include_package_data=True,
	zip_safe=False,
	install_requires=[],
	entry_points=\
	"""
        [ckan.plugins]
	    scos_pod_harvester=ckanext.scos_pod_harvester.plugin:SCOS_POD_harvester
	""",
)
